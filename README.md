# Billion CI Attack

What happens when a CI/CD pipeline indefinitely and recursively re-triggers itself? This project makes it easy to discover that for yourself, **on your self-hosted GitLab server** of course.

Set `$SETTINGS_CICD_PIPELINE_TRIGGERS_TOKEN` to a token obtained through Settings >> CI/CD >> Pipeline triggers. `$CI_JOB_TOKEN` works as well but traces upstream / downstream jobs which come with additional limits.

See
1. https://en.wikipedia.org/wiki/Billion_laughs_attack
1. https://docs.gitlab.com/runner/configuration/advanced-configuration.html
1. https://www.howtogeek.com/devops/how-to-manage-gitlab-runner-concurrency-for-parallel-ci-jobs/

<details>
<summary>Spoiler alert:</summary>

1. GitLab Runners offer good rate limiting: CPU isn't an issue.
2. Pipelines are created quickly, but remain in that initial state for a long time.
3. My GitLab instance became unresponsive after a while!
4. It turned out that `/` filled up 100% of the available disk, mostly consisting of logs!
</details>
